import React, { Component } from 'react';
import { Linking} from 'react-native'
import { Button,Button2, StyleSheet, View } from 'react-native';

export default class ButtonBasics extends Component {
  _onPressButton() {
    alert('YOU PRESSED USER INTERACTION BUTTON!')
    Linking.openURL('https://youtube.com');
  }
  _onPressButton2() {
    alert('Hello Pakistan!')
  }

  render() {
    return (
      <View >
        <View >
          <Button
            onPress={this._onPressButton}
            title="User Ineraction"
          />
        </View>
        <View >
          <Button
            onPress={this._onPressButton2}
            title="Press Me"
            color="#841584"
          />
        </View>
      </View>
    );
  }
}
